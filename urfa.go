package urfa

import (
	"bytes"
	"crypto/md5"
	"crypto/tls"
	"encoding/binary"
	"fmt"
	"io"
	"net"
	"sync"
)

const (
	RC_SESSION_INIT   uint8 = 192
	RC_ACCESS_REQUEST uint8 = 193
	RC_ACCESS_ACCEPT  uint8 = 194
	RC_ACCESS_REJECT  uint8 = 195
	RC_SESSION_DATA   uint8 = 200
	RC_SESSION_CALL   uint8 = 201
	RC_SESSION_END    uint8 = 203

	attrUserType      attrK = 1
	attrUserName      attrK = 2
	attrCall          attrK = 3
	attrEnd           attrK = 4
	attrData          attrK = 5
	attrKey           attrK = 6
	attrClientIp      attrK = 7
	attrChapChallenge attrK = 8
	attrChapResponse  attrK = 9
	attrSslRequest    attrK = 10

	userTypeUser    userType = 0
	userTypeService userType = 1
	userTypeCard    userType = 2

	sslTypeNone      sslType = 0
	sslTypeSSL3      sslType = 2
	sslTypeCERT      sslType = 3
	sslTypeSSL3Admin sslType = 4
	sslTypeTLS1Admin sslType = 5
	sslTypeTLS1      sslType = 6

	stateNone   state = 0
	stateInput  state = 1
	stateOutput state = 2
)

type state int
type sslType int32
type userType int
type attrK int16
type H map[string]interface{}

type Urfa struct {
	sync.Mutex
	host      string
	username  string
	password  string
	conn      net.Conn
	state     state
	sslType   sslType
	userType  userType
	sessionID string
	clientIP  net.IP
	isAdmin   bool
}

func (u *Urfa) connect() (err error) {
	if u.conn, err = net.Dial("tcp", u.host); err != nil {
		return
	}
	if err = u.login(); err != nil {
		return
	}
	return
}

func (u *Urfa) reconnect() (err error) {
	u.Close()
	err = u.connect()
	return
}

func (u *Urfa) login() (err error) {
	var p *packet
	if p, err = readPacket(u); err != nil {
		err = fmt.Errorf("auth: recvPacket 1 failed")
		return
	}
	if p.code != RC_SESSION_INIT {
		err = fmt.Errorf("auth: packet code != RC_SESSION_INIT")
		return
	}
	keyAttr := p.getAttr(attrKey)
	if keyAttr == nil {
		err = fmt.Errorf("auth: attrKey not found")
		return
	}
	if len(keyAttr.data) != 16 {
		err = fmt.Errorf("auth: attrKey len != 16")
		return
	}
	p.clean()
	p.code = RC_ACCESS_REQUEST
	p.addAttr(attrUserName, u.username)
	hash := md5.New()
	io.WriteString(hash, string(keyAttr.data))
	io.WriteString(hash, u.password)
	p.addAttr(attrChapResponse, string(hash.Sum(nil)))
	p.addAttr(attrChapChallenge, string(keyAttr.data))
	if u.clientIP != nil {
		p.addAttr(attrClientIp, u.clientIP)
	}
	// restore session
	if u.sessionID != "" && u.clientIP != nil {
		p.addAttr(attrKey, u.sessionID)
	}
	if u.userType != userTypeUser {
		p.addAttr(attrUserType, u.userType)
	}
	p.addAttr(attrSslRequest, u.sslType)
	if err = sendPacket(u, p); err != nil {
		err = fmt.Errorf("auth: sendPacket failed")
		return
	}
	p.clean()
	if p, err = readPacket(u); err != nil {
		err = fmt.Errorf("auth: recvPacket 2 failed")
		return
	}
	if p.code != RC_ACCESS_ACCEPT {
		err = fmt.Errorf("auth: wrong username or password p.code !=RC_ACCESS_ACCEPT")
		return
	}
	sslRequestAttr := p.getAttr(attrSslRequest)
	if sslRequestAttr != nil {
		if toSslType(sslRequestAttr.data) == sslTypeSSL3 {
			if err = u.enableTLSv1(); err != nil {
				return
			}
		}
		if toSslType(sslRequestAttr.data) == sslTypeSSL3Admin {
			err = fmt.Errorf("auth: sslTypeSSL3Admin not supported")
			return
		}
		if toSslType(sslRequestAttr.data) == sslTypeTLS1 {
			if err = u.enableTLSv1(); err != nil {
				return
			}
		}
		if toSslType(sslRequestAttr.data) == sslTypeTLS1Admin {
			if err = u.enableTLSv1Admin(); err != nil {
				return
			}
		}
	}
	if u.sessionID == "" {
		u.sessionID = string(keyAttr.data)
	}
	return
}

func (u *Urfa) enableTLSv1() (err error) {
	tlsConf := &tls.Config{
		//CipherSuites: []uint16{tls.TLS_RSA_WITH_AES_128_CBC_SHA},
		InsecureSkipVerify: true,
	}
	u.conn = tls.Client(u.conn, tlsConf)
	err = u.conn.(*tls.Conn).Handshake()
	return
}

func (u *Urfa) enableTLSv1Admin() (err error) {
	var tlsCert tls.Certificate
	if tlsCert, err = tls.X509KeyPair([]byte(CERT), []byte(CERT)); err != nil {
		return
	}
	tlsConf := &tls.Config{
		Certificates:       []tls.Certificate{tlsCert},
		InsecureSkipVerify: true,
	}
	u.conn = tls.Client(u.conn, tlsConf)
	err = u.conn.(*tls.Conn).Handshake()
	return
}

func (u *Urfa) Close() {
	if u.conn.Close() != nil {
		u.conn.Close()
	}
	return
}

func (u *Urfa) SessionID() string{
	return u.sessionID
}

func (u *Urfa) Call(code int32) (err error) {
	p := newPacket()
	p.code = RC_SESSION_CALL
	p.addAttr(attrCall, code)
	if err = sendPacket(u, p); err != nil {
		return
	}
	if p, err = readPacket(u); err != nil {
		return
	}
	attr := p.getAttr(attrEnd)
	if attr != nil {
		err = fmt.Errorf("Call func 0x%x err: %s", code, attr.data)
		return
	}
	return
}

func (u *Urfa) Ping() bool {
	u.Lock()
	defer u.Unlock()
	if err := u.Call(0x00001); err != nil {
		return false
	}
	return true
}

func (u *Urfa) PingWithReconnect() (ok bool, err error) {
	if ok = u.Ping(); ok {
		return
	}
	if err = u.reconnect(); err != nil {
		return
	}
	ok = true
	return
}

func (u *Urfa) GetReader() (r *reader, err error) {
	return newReader(u)
}

func (u *Urfa) GetPacket() *packet {
	return newPacket()
}

func (u *Urfa) SendParam(p *packet) (err error) {
	p.code = RC_SESSION_DATA
	err = sendPacket(u, p)
	return
}

func NewClient(host, username, password string) (u *Urfa, err error) {
	u = &Urfa{
		Mutex:    sync.Mutex{},
		host:     host,
		username: username,
		password: password,
		userType: userTypeUser,
		sslType:  sslTypeTLS1Admin,
		isAdmin:  false,
	}
	err = u.connect()
	return
}

func NewAdminClient(host, username, password string) (u *Urfa, err error) {
	u = &Urfa{
		Mutex:    sync.Mutex{},
		host:     host,
		username: username,
		password: password,
		userType: userTypeUser,
		sslType:  sslTypeTLS1Admin,
		isAdmin:  true,
	}
	err = u.connect()
	return
}

func toSslType(data []byte) (v sslType) {
	buf := bytes.NewReader(data)
	binary.Read(buf, binary.BigEndian, &v)
	return
}
