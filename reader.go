package urfa

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

type reader struct {
	urfa   *Urfa
	iter   int32
	packet *packet
}

func newReader(u *Urfa) (r *reader, err error) {
	r = &reader{u, 0, newPacket()}
	err = r.getpacket()
	return
}

func (r *reader) getpacket() (err error) {
	if r.iter >= int32(len(r.packet.data)) {
		r.iter = 0
		r.packet, err = readPacket(r.urfa)
		if err != nil {
			return
		}
		if r.packet.getAttr(attrEnd) != nil {
			return fmt.Errorf("Data done")
		}
	}
	return
}

func (r *reader) Clean() {
	for {
		r.packet, _ = readPacket(r.urfa)
		if r.packet.getAttr(attrEnd) != nil {
			break
		}
	}
}

func (r *reader) GetInteger(v *int32) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	buf := bytes.NewReader(r.packet.data[r.iter])
	binary.Read(buf, binary.BigEndian, v)
	r.iter++
	return
}

func (r *reader) GetDouble(v *float64) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	buf := bytes.NewReader(r.packet.data[r.iter])
	binary.Read(buf, binary.BigEndian, v)
	r.iter++
	return
}

func (r *reader) GetLong(v *int64) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	buf := bytes.NewReader(r.packet.data[r.iter])
	binary.Read(buf, binary.BigEndian, v)
	r.iter++
	return
}

func (r *reader) GetIpAddress(v *net.IP) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	if len(r.packet.data[r.iter]) == 4 {
		*v = net.IP(r.packet.data[r.iter])
	} else if int(r.packet.data[r.iter][0]) == 4 {
		*v = net.IP(r.packet.data[r.iter][1:]).To4()
	} else if int(r.packet.data[r.iter][0]) == 6 {
		*v = net.IP(r.packet.data[r.iter][1:]).To16()
	}
	r.iter++
	return
}

func (r *reader) GetString(v *string) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	*v = string(r.packet.data[r.iter])
	r.iter++
	return
}

func (r *reader) GetTime(v *time.Time) (err error) {
	if err = r.getpacket(); err != nil {
		return
	}
	var intTime int32
	buf := bytes.NewReader(r.packet.data[r.iter])
	binary.Read(buf, binary.BigEndian, &intTime)
	*v = time.Unix(int64(intTime), 0)
	r.iter++
	return
}
