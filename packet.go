package urfa

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"time"
)

type attr struct {
	data []byte
	len  uint16
}

type packet struct {
	code    uint8
	version uint8
	len     uint16
	attrs   map[attrK]attr
	data    [][]byte
}

func newPacket() *packet {
	return &packet{0, 35, 4, make(map[attrK]attr), [][]byte{}}
}

func (p *packet) clean() {
	p.code = 0
	p.version = 35
	p.len = 4
	p.attrs = make(map[attrK]attr)
	p.data = [][]byte{}
}

func (p *packet) getAttr(attr attrK) *attr {
	a, ok := p.attrs[attr]
	if ok {
		return &a
	}
	return nil
}

func (p *packet) addAttr(code attrK, data interface{}) (err error) {
	switch a := data.(type) {
	case string:
		buf := new(bytes.Buffer)
		buf.WriteString(a)
		p.attrs[code] = attr{buf.Bytes(), uint16(len(a) + 4)}
		p.len += uint16((len(a) + 4))
	case int32, sslType, userType:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, a)
		p.attrs[code] = attr{buf.Bytes(), 8}
		p.len += 8
	default:
		err = fmt.Errorf("addAttr not allow data type %t", a)
	}
	return
}

func (p *packet) addData(data interface{}) (err error) {
	switch d := data.(type) {
	case int32:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, d)
		p.data = append(p.data, buf.Bytes())
		p.len += 8
	case float64:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, d)
		p.data = append(p.data, buf.Bytes())
		p.len += 12
	case string:
		p.data = append(p.data, []byte(d))
		p.len += uint16(len(d)) + 4
	case time.Time:
		buf := new(bytes.Buffer)
		binary.Write(buf, binary.BigEndian, int32(d.Unix()))
		p.data = append(p.data, buf.Bytes())
		p.len += 8
	default:
		err = fmt.Errorf("addData not allow data type %t", d)
	}
	return
}

func (p *packet) AddDataInteger(i int32) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, i)
	p.data = append(p.data, buf.Bytes())
	p.len += 8
	return
}

func (p *packet) AddDataDouble(f float64) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, f)
	p.data = append(p.data, buf.Bytes())
	p.len += 12
	return
}

func (p *packet) AddDataString(s string) {
	p.data = append(p.data, []byte(s))
	p.len += uint16(len(s)) + 4
	return
}

func (p *packet) AddDataTime(t time.Time) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, int32(t.Unix()))
	p.data = append(p.data, buf.Bytes())
	p.len += 8
	return
}

func readPacket(u *Urfa) (p *packet, err error) {
	p = newPacket()
	if err = binary.Read(u.conn, binary.BigEndian, &p.code); err != nil {
		return
	}
	if err = binary.Read(u.conn, binary.BigEndian, &p.version); err != nil {
		return
	}
	if err = binary.Read(u.conn, binary.BigEndian, &p.len); err != nil {
		return
	}
	tmplen := uint16(4)
	var code attrK
	var lenght uint16
	for tmplen < p.len {
		if err = binary.Read(u.conn, binary.LittleEndian, &code); err != nil {
			return
		}
		if err = binary.Read(u.conn, binary.BigEndian, &lenght); err != nil {
			return
		}
		tmplen += lenght
		if code == attrData {
			data := make([]byte, lenght-4)
			if _, err = u.conn.Read(data); err != nil {
				return
			}
			p.data = append(p.data, data)
		} else {
			p.attrs[code] = attr{make([]byte, lenght-4), lenght}
			if _, err = u.conn.Read(p.attrs[code].data); err != nil {
				return
			}
		}
	}
	return
}

func sendPacket(u *Urfa, p *packet) (err error) {
	buf := new(bytes.Buffer)
	buf.WriteByte(byte(p.code))
	buf.WriteByte(byte(p.version))
	binary.Write(buf, binary.BigEndian, p.len)
	for code, attr := range p.attrs {
		binary.Write(buf, binary.LittleEndian, uint16(code))
		binary.Write(buf, binary.BigEndian, attr.len)
		buf.Write(attr.data)
	}
	for _, d := range p.data {
		binary.Write(buf, binary.LittleEndian, uint16(5))
		binary.Write(buf, binary.BigEndian, uint16(len(d)+4))
		buf.Write(d)
	}
	if _, err = u.conn.Write(buf.Bytes()); err != nil {
		log.Println("Error wrinte")
		return
	}
	return
}
